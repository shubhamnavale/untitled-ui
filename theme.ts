import theme from "./theme.json";
import { ColorVariantType } from "./types/colors";

const baseColors = ["white", "black"];
const primaryColors = [
    ...baseColors,
    "gray",
    "primary",
    "error",
    "warning",
    "success",
];
const secondaryColors = [
    "gray-blue",
    "gray-cool",
    "gray-modern",
    "gray-neutral",
    "gray-iron",
    "gray-true",
    "gray-warm",
    "moss",
    "green-light",
    "green",
    "teal",
    "cyan",
    "blue-light",
    "blue",
    "blue-dark",
    "indigo",
    "violet",
    "purple",
    "fuchsia",
    "pink",
    "rose",
    "orange-dark",
    "orange",
    "yellow",
];

const colorVariantDepths = [
    25, 50, 100, 200, 300, 400, 500, 600, 700, 800, 900,
];

export type ColorVariantsMapType = { [key in ColorVariantType]: string };

export const colorVariantsMap = {
    ...baseColors.reduce(
        (acc, color: string) => ({
            ...acc,
            [color]: theme.colors[color as keyof typeof theme.colors],
        }),
        {}
    ),
    ...primaryColors.reduce(
        (acc, color) =>
            baseColors.includes(color as string)
                ? {}
                : {
                      ...acc,
                      [color]:
                          theme.colors[color as keyof typeof theme.colors][
                              "400"
                          ],
                      ...colorVariantDepths.reduce(
                          (acc, depth) => ({
                              ...acc,
                              [`${color}-${depth}`]:
                                  theme.colors[
                                      color as keyof typeof theme.colors
                                  ][
                                      `${depth}` as keyof typeof theme.colors.primary
                                  ],
                          }),
                          {}
                      ),
                  },
        {}
    ),
    ...secondaryColors.reduce(
        (acc, color) => ({
            ...acc,
            [color]: theme.colors[color as keyof typeof theme.colors]["400"],
            ...colorVariantDepths.reduce(
                (acc, depth) => ({
                    ...acc,
                    [`${color}-${depth}`]:
                        theme.colors[color as keyof typeof theme.colors][
                            `${depth}` as keyof typeof theme.colors.primary
                        ],
                }),
                {}
            ),
        }),
        {}
    ),
} as ColorVariantsMapType;
