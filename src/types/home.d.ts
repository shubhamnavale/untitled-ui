export interface HeaderLeftContentProps {
    data: {
        title: string;
        subtitle?: string;
    };
}

export interface CardProps {
    data: {
        title: string;
        value: string;
        status: {
            label: string;
            isComplete: boolean;
        }[];
        directionMetric: {
            description: string;
            data: {
                value: number;
                limit: number;
            };
        };
    }[];
}
