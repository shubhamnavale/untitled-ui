export interface SettingsFormProps {
    formData: {
        title: string;
        subTitle: string;
        type: string;
    }[];
}
