import React, { FC } from "react";
import Avatar from "../avatar/avatar";
import Typography, { TypographySizeType } from "../typography/typography";

export interface UserProfileProps {
    name: string;
    subTitle: string;
    avatar?: string;
    status?: "online" | "offline" | "busy" | "away";
    size?: "sm" | "md" | "lg" | "xl";
}

const nameSizes = {
    sm: "sm",
    md: "sm",
    lg: "md",
    xl: "lg",
};

const roleSizes = {
    sm: "xs",
    md: "sm",
    lg: "md",
    xl: "md",
};

export const UserProfile: FC<UserProfileProps> = ({
    name,
    subTitle,
    avatar,
    status = "away",
    size = "md",
}) => {
    return (
        <div className="flex flex-grow">
            {avatar && (
                <Avatar src={avatar} name={name} status={status} size={size} />
            )}
            <div className="ml-3 text-left">
                <Typography
                    size={nameSizes[size] as TypographySizeType}
                    weight="medium"
                    color="gray-700"
                >
                    {name}
                </Typography>
                <Typography
                    size={roleSizes[size] as TypographySizeType}
                    color="gray-500"
                >
                    {subTitle}
                </Typography>
            </div>
        </div>
    );
};

export default UserProfile;
