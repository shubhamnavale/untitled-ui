import type { ComponentStory, ComponentMeta } from "@storybook/react";
import { SideBar } from "./sidebar";

const Story: ComponentMeta<typeof SideBar> = {
    component: SideBar,
    title: "SideBar",
};
export default Story;

const Template: ComponentStory<typeof SideBar> = args => (
    <SideBar {...(args as any)} />
);

export const Regular = Template.bind({});
Regular.args = {};
