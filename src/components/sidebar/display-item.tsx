import { FC } from "react";
import Badge from "../badge/badge";
import { Icon, IconType } from "../icon";
import Typography from "../typography/typography";
import React from "react";

export interface DisplayItemProps extends React.HTMLProps<HTMLDivElement> {
    title: string;
    open?: boolean;
    selected?: boolean;
    dropdown?: boolean;
    badgeValue?: string;
    icon?: IconType;
    children?: React.ReactNode;
}

export const DisplayItem: FC<DisplayItemProps> = ({
    title,
    badgeValue,
    icon,
    children = null,
    open = false,
    selected = false,
    dropdown = false,
    className = "",
    ...restProps
}) => {
    if (children) {
        return <>{children}</>;
    }

    return (
        <div
            {...restProps}
            className={`group flex items-center px-3 py-2 mb-1 rounded-md cursor-pointer ${
                (selected || open) && "bg-gray-50"
            } hover:bg-gray-50 ${className}`}
        >
            {icon ? (
                <Icon
                    name={icon}
                    size={20}
                    color="gray-500"
                    className="mr-3 group-hover:stroke-gray-700"
                />
            ) : (
                <div className="w-5 mr-3" />
            )}
            <div className="flex-grow flex items-center">
                <Typography
                    weight="medium"
                    color="gray-700"
                    className="text-gray-700 group-hover:text-gray-900"
                >
                    {title}
                </Typography>
                {badgeValue && (
                    <Badge
                        label={badgeValue}
                        className="bg-gray-100 text-gray-700 ml-auto"
                    />
                )}
            </div>
            {dropdown && (
                <Icon
                    name="chevron-right"
                    size={20}
                    color={open ? "gray-400" : "gray-300"}
                    className={`${
                        open ? "rotate-90" : ""
                    } ml-3 flex-shrink-0 transform transition-colors duration-150 ease-in-out group-hover:stroke-gray-400 flex`}
                />
            )}
        </div>
    );
};

export default DisplayItem;
