import { ColorVariantType } from "../../types/colors";

export type IconType =
    | "activity"
    | "arrow-down"
    | "arrow-left"
    | "arrow-right"
    | "arrow-up"
    | "bar-line-chart"
    | "check-circle"
    | "check-square"
    | "chevron-down"
    | "chevron-right"
    | "clock-fast-forward"
    | "eye"
    | "file-04"
    | "file-07"
    | "help-circle"
    | "home-line"
    | "life-buoy-01"
    | "lightbulb-03"
    | "log-out-01"
    | "message-smile-circle"
    | "minus-square"
    | "notification-box"
    | "pie-chart-03"
    | "placeholder"
    | "plus"
    | "search-lg"
    | "settings-01"
    | "settings-03"
    | "square"
    | "star-01"
    | "trash-01"
    | "upload-cloud-02"
    | "user-01"
    | "user-square"
    | "x-close";

export interface IconProps {
    name: IconType;
    color?: ColorVariantType;
    size?: number;
    className?: string;
    onClick?: () => void;
}
