import type { ComponentStory, ComponentMeta } from "@storybook/react";
import { DirectionMetric } from "./direction-metric";

const Story: ComponentMeta<typeof DirectionMetric> = {
    component: DirectionMetric,
    title: "DirectionMetric",
};
export default Story;

const Template: ComponentStory<typeof DirectionMetric> = args => (
    <DirectionMetric {...args} />
);

export const Primary = Template.bind({});
Primary.args = {};
