import type { ComponentStory, ComponentMeta } from "@storybook/react";
import { TextField } from "./text-field";

const Story: ComponentMeta<typeof TextField> = {
    component: TextField,
    title: "TextField",
};
export default Story;

const Template: ComponentStory<typeof TextField> = args => (
    <TextField {...args} />
);

const Primary = Template.bind({});
Primary.args = {};
