/* eslint-disable-next-line */
import React from "react";
import { twMerge } from "tailwind-merge";

/**
 * Defines the props for the TextField component
 * @param color - The color of the TextField
 * @param disabled - Whether the TextField is disabled
 * @param error - Whether the TextField has an error
 * @param helperText - Helper text to display below the TextField
 * @param id - Id to be used for unique identification
 * @param label - Label to be displayed above the TextField
 * @param name - Name to be used for unique identification
 * @param placeholder - Placeholder text to be displayed inside the TextField
 * @param type - TextField type (text, password, email, etc.)
 * @param value - Value of the TextField
 * @param variant - Variant of the TextField (filled, outlined, standard)
 * @param size - Size of the TextField (small, normal)
 * @param fullWidth - Whether the TextField should take up the full width of its container
 * @param multiline - Whether the TextField should be multiline (textarea)
 * @param select - Whether the TextField should be a select (dropdown)
 * @param options - Options to be displayed in the select (dropdown)
 * @param className - Additional class names to be added to the TextField
 * @param restProps - Additional props to be spread to the TextField
 */
export interface TextFieldProps {
    className?: string;
    color?:
        | "primary"
        | "secondary"
        | "danger"
        | "success"
        | "info"
        | "surface"
        | "on-surface"
        | "warning";
    disabled?: boolean;
    error?: boolean;
    fullWidth?: boolean;
    helperText?: string;
    id?: string;
    label?: string;
    multiline?: boolean;
    name?: string;
    options?: string[];
    placeholder?: string;
    required?: boolean;
    select?: boolean;
    size?: "normal" | "small";
    type?: string;
    value?: string;
    variant?: "filled" | "outlined" | "default";
}
/**
 * @param {TextFieldProps} props - The props for the TextField component
 * @returns TextField component
 * @description This component is used to render a TextField which could be a text input, password input, email input, textarea, or select (dropdown)
 * @example <TextField color="primary" />
 */
export function TextField({
    color = "primary",
    disabled,
    error,
    helperText,
    id,
    label,
    name,
    placeholder,
    type,
    value,
    variant = "default",
    size = "normal",
    fullWidth,
    multiline,
    select,
    options,
    className,
    ...restProps
}: TextFieldProps) {
    const variants = {
        outlined: `border-2 rounded ${
            error
                ? `border-danger`
                : `border-secondary hover:border-onSurface focus:border-${color}`
        }`,
        filled: `border-b-2 rounded-t ${
            error
                ? `border-danger bg-danger/20`
                : `border-secondary hover:border-onSurface focus:border-${color} bg-${color}/20`
        }`,
        default: `border-b-2 ${
            error
                ? `border-danger`
                : `border-secondary hover:border-onSurface focus:border-${color}`
        }`,
    };

    const sizes = {
        small: "h-8 px-2",
        normal: "h-10 px-4",
    };

    const inputClasses = twMerge(
        `outline-none`,
        fullWidth ? `w-full` : `w-fit`,
        sizes[size],
        variants[variant],
        className
    );

    const textFieldDisplay = React.createElement(
        select ? "select" : multiline ? "textarea" : "input",
        {
            ...restProps,
            className: `${inputClasses}`,
        },
        select
            ? options?.map((option, i) => (
                  <option value={option} key={i}>
                      {option}
                  </option>
              ))
            : null
    );

    return (
        <div
            className={`flex flex-col ${
                disabled && "opacity-50 cursor-not-allowed"
            }`}
        >
            {label && (
                <label
                    htmlFor={id}
                    className={`block text-sm font-medium ${
                        error ? `text-danger` : `text-secondary`
                    } `}
                >
                    {label}
                </label>
            )}
            {textFieldDisplay}
            {helperText && (
                //TODO: Fix hardcoded text.
                <p
                    className={`text-sm ${
                        error ? `text-danger` : `text-secondary`
                    }`}
                >
                    {helperText}
                </p>
            )}
        </div>
    );
}
export default TextField;
