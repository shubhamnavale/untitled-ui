import type { ComponentStory, ComponentMeta } from "@storybook/react";
import { IconBadge } from "./icon-badge";
import { colorVariantsMap } from "../../theme";

const Story: ComponentMeta<typeof IconBadge> = {
    component: IconBadge,
    title: "IconBadge",
};
export default Story;

const Template: ComponentStory<typeof IconBadge> = args => (
    <IconBadge {...args} />
);

export const Regular = Template.bind({});
Regular.args = {
    color: "success",
    icon: "x-close",
};
