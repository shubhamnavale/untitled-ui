import type { ComponentStory, ComponentMeta } from "@storybook/react";
import { UploadItem } from "./upload-item";

const Story: ComponentMeta<typeof UploadItem> = {
    component: UploadItem,
    title: "UploadItem",
};
export default Story;

const Template: ComponentStory<typeof UploadItem> = args => (
    <UploadItem {...(args as any)} />
);

export const Regular = Template.bind({});
Regular.args = {};
