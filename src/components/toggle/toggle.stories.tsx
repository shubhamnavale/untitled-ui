import type { ComponentStory, ComponentMeta } from "@storybook/react";
import { Toggle } from "./toggle";

const Story: ComponentMeta<typeof Toggle> = {
    component: Toggle,
    title: "Toggle",
};
export default Story;

const Template: ComponentStory<typeof Toggle> = args => (
    <Toggle {...(args as any)} />
);

export const Regular = Template.bind({});
Regular.args = {};
