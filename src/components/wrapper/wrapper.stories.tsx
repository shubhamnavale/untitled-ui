import type { ComponentStory, ComponentMeta } from "@storybook/react";
import { Wrapper } from "./wrapper";

const Story: ComponentMeta<typeof Wrapper> = {
    component: Wrapper,
    title: "Wrapper",
};
export default Story;

const Template: ComponentStory<typeof Wrapper> = args => <Wrapper {...args} />;

const Primary = Template.bind({});
Primary.args = {};
