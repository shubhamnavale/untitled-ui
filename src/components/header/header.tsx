import { HeaderLeftContentProps } from "../../types/home";
import Button from "../button/button";
import HeaderLeft from "./header-left";
import React from "react";

const Header = ({ data }: HeaderLeftContentProps) => {
    return (
        <div className="lg:flex items-center justify-between">
            {/* content in left section */}
            <HeaderLeft data={data} />
            {/* content in right section */}
            <div className="flex items-center gap-3 mt-8 lg:mt-0">
                <Button
                    variant="outlined"
                    label="This month"
                    icon="clock-fast-forward"
                />
                <Button variant="outlined" label="Tue 1 Sep - Wed 14 Sep" />
            </div>
        </div>
    );
};

export default Header;
