import { HeaderLeftContentProps } from "../../types/home";
import Typography from "../typography/typography";
import React from "react";

const HeaderLeft = ({ data }: HeaderLeftContentProps) => {
    return (
        <div className=" flex flex-col gap-1">
            <Typography variant="h1" weight="medium" color="gray-900">
                {data.title}
            </Typography>
            {data.subtitle && (
                <Typography color="gray-500">{data.subtitle}</Typography>
            )}
        </div>
    );
};

export default HeaderLeft;
