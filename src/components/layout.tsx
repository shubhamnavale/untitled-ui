import React from "react";
import SideBar from "./sidebar/sidebar";

const Layout = ({ children }: { children: React.ReactNode }) => {
    return (
        <div className="flex h-screen">
            {/* sidebar for tablet & desktop - static in nature throughout all the pages */}
            <SideBar />
            {/* children is the content of the page */}
            <main className="flex-1 py-6 space-y-8 mx-auto max-w-7xl w-full h-full overflow-auto px-4 sm:px-6 md:px-8">
                {children}
            </main>
        </div>
    );
};

export default Layout;
