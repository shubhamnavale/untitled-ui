import React from "react";
import Button from "../button/button";
import Typography from "../typography/typography";

const SubHeader = () => {
    return (
        <div className="flex items-center justify-between">
            <div className="space-y-1">
                <Typography size="lg" weight="medium" color="gray-900">
                    Company profile
                </Typography>
                <Typography size="sm" color="gray-500">
                    Update your company photo and details here.
                </Typography>
            </div>
            <div className="flex items-center gap-2">
                <Button variant="outlined" color="gray" label="Cancel" />
                <Button label="Save" />
            </div>
        </div>
    );
};

export default SubHeader;
